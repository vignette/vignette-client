import Html exposing (Html, button, input, br, div, p, text)
import Html.Events exposing (onClick, onInput)
import Html.Attributes exposing (placeholder, align, value, attribute)
import Debug exposing (log)

main =
  Html.beginnerProgram { model = initialModel, view = view, update = update }

type Msg = 
  TemplateFileChange String
  | SinglesCountChange String
  | SinglesFileChange String
  | Send

type alias SVGTemplate =
  { templateFile : Maybe String
  , singlesCount : Maybe Int
  , singleFile : Maybe String
  , singlesFile : Maybe String
  }

update : Msg -> SVGTemplate -> SVGTemplate
update msg model =
  case msg of
      Send ->
        log "model" model
      SinglesFileChange name ->
        {model | singlesFile = Just name }
      SinglesCountChange scount ->
        if String.length scount == 0 then
          {model | singlesCount = Nothing }          
        else case String.toInt scount of
              Err _ -> model
              Ok val -> {model | singlesCount = Just val }
      TemplateFileChange name ->
        { model | templateFile = Just name }
          
view : SVGTemplate -> Html Msg
view model =
  div [ align "center"]
    [ p [] []
    , input [placeholder "Template", onInput TemplateFileChange, value (Maybe.withDefault "" model.templateFile), attribute "class" (colorMaybe model.templateFile) ] []
    , br [] []
    , input [placeholder "SinglesFile", onInput SinglesFileChange, value (Maybe.withDefault "" model.singlesFile) ] []
    , br [] []
    , input [placeholder "SinglesPerFile", onInput SinglesCountChange, value (intToStr model.singlesCount) ] []
    , p [] []
    , button [ onClick Send ] [ text "Send" ]
    ]

initialModel : SVGTemplate
initialModel = {
  templateFile =  Just "A4-Portfait-6.svg"
  , singlesCount = Just 6
  , singleFile = Nothing
  , singlesFile = Nothing
  }

intToStr : Maybe Int -> String
intToStr m =
  case m of
    Nothing -> ""
    Just val -> toString val

validateFileName : String -> String -> Maybe String
validateFileName name extension =
  if String.endsWith name extension == True then
    Debug.log "ok" Just name
  else Debug.log "nw" Nothing

colorMaybe : Maybe String -> String
colorMaybe m =
  if m == Nothing then
    "invalid"
  else
    "valid"